Package: dpseg
Title: Piecewise Linear Segmentation by Dynamic Programming
Version: 0.1.1
Authors@R: c(person("Rainer","Machne", role = c("aut", "cre"),
                   email = "machne@hhu.de",
                   comment = c(ORCID = "0000-0002-1274-5099")),
            person("Peter F.", "Stadler", role = c("aut")))
Maintainer: Rainer Machne <machne@hhu.de>
BugReports: https://gitlab.com/raim/growthphases/-/issues
Description: Piecewise linear segmentation of ordered data by a
        dynamic programing algorithm. The algorithm developed for time
	series data, eg. growth curves, and for genome-wide read-count data
        from next generation sequencing, but is broadly applicable.
        Generic implementations of dynamic programing routines allow
        to scan for optimal segmentation parameters and test custom
        segmentation criteria ("scoring functions").
Depends: 
    R (>= 3.0.0)
Imports: Rcpp (>= 0.12.18)
Suggests:
    knitr,
    htmltools,
    microbenchmark,
    ggplot2
LinkingTo: Rcpp
URL: https://gitlab.com/raim/growthphases/
License: GPL (>= 2)
Encoding: UTF-8
LazyData: true
VignetteBuilder: knitr
RoxygenNote: 7.3.2
