
### EXPERIMENTAL: parameter estimation and scans

## estimate good range for P, when using variance of residuals,
## NOTE: takeing 1/100 of this worked for OD4 data from 20200629_topA
#' Estimate a good value for penalty P
#'
#' P should be in the range of expected variances. To find a good
#' initial estimate for P, the data is smoothed by \code{\link{smooth.spline}}
#' and the variance of residuals reported.
#' @param x x-values
#' @param y y-values
#' @param plot plot the \code{\link{smooth.spline}}
#' @param ... parameters for \code{\link{smooth.spline}}
#' @export
estimateP <- function(x, y, plot=FALSE, ...) {
    rm <- is.na(y)|is.infinite(y)
    x <- x[!rm]
    y <- y[!rm]
    ## NOTE: predict is necessary because smooth.spline
    ## removes ties
    ys <- predict(smooth.spline(x, y, ...), x=x)$y
    if ( plot ) {
        plot(x, y, cex=1, pch=19, col="#00000099")
        lines(x, ys, col="red", lwd=1.5)
    }
    var(ys -y)
}

#' Scan over different penalty \code{P} values
#'
#' This runs \code{\link{dpseg}} for different values of the penalty
#' parameter \code{P} from 0 to \code{Pmax} and returns a matrix
#' with two columns, the used \code{P} values and the resulting number
#' of segments and (optionally) the median of segment variance of residuals.
#' @param x x-values
#' @param y y-values
#' @param Pmax maximal \code{P}
#' @param l number of \code{P} to test: \code{seq(0,Pmax,length.out=l)}
#' @param var add the median of the variances of residuals of all segments
#' to output
#' @param use.matrix use the stored scoring function matrix for more efficient
#' scan; set this to \code{FALSE} if you run into memory problems
#' @param plot plot results
#' @param verb print progress messages
#' @param ... parameters for \code{\link{dpseg}} (except \code{P})
#'@export
scanP <- function(x, y, Pmax, l=50, var=TRUE, use.matrix=TRUE,
                  plot=TRUE, verb=TRUE, ...) {

    if ( missing(Pmax) )
        Pmax <- estimateP(x=x, y=y)
    ps <- seq(0,Pmax,length.out=l)
    nsg <- rep(NA, length(ps))
    if ( var )
        vars <- nsg
    
    if ( verb )
        cat(paste("running dpseg", length(ps), "times: "))
    
    ## if enough memory is available, we can run this
    ## more efficiently by storing the scoring matrix
    if ( use.matrix ) {
        ## run once with store-matrix
        dp <- dpseg(x=x, y=y, P=ps[1], store.matrix=TRUE, verb=0, ...)

        if ( verb ) cat(".")

        nsg[1] <- nrow(dp$segments)
        if ( var )
            vars[1] <- median(dp$segments$var)
        
        ## and rerun using pre-calculated matrix
        for ( i in 2:length(ps) ) {
            dpi <- dpseg(y=dp$SCR, P=ps[i], store.matrix=FALSE, verb=0, ...)
            nsg[i] <- nrow(dpi$segments)
            if ( var ) {
                dpi <- addLm(dpi, x=x, y=y)
                vars[i] <-  median(dpi$segments$var.lm)
            }
            if ( verb ) cat(".")
        }
    } else {
        for ( i in 1:length(ps) ) {
            dpi <- dpseg(x=x, y=y, P=ps[i],
                         store.matrix=FALSE, verb=0, ...)
            nsg[i] <- nrow(dpi$segments)
            if ( var )
                vars[i] <-  median(dpi$segments$var)
            if ( verb ) cat(".")
        }
    }
    if ( verb ) cat("\n")
    res <- cbind(P=ps,"number of segments"=nsg)
    if ( var )
        res <- cbind(res, "median of variances"=vars)
    if ( plot ) {
        plot(res, type="l")
        points(res, pch=19, cex=.5)
        if ( var ) {
            par(new=TRUE)
            plot(res[,1],res[,3], type="l", col=2,axes=FALSE,xlab=NA,ylab=NA)
            points(res[,1], res[,3], col=2, pch=19, cex=.5)
            axis(4,col=2,col.axis=2)
            mtext("median of variances", 4, par("mgp")[1], col=2)
        }
            
    }
    invisible(res)
}


### SCORING FUNCTIONS
## for generic recursion S[i-jumps] + score(i,j)
get_scoreij <- function(type) 
    get(paste0("scoreij","_",type), mode="function")
scoreij_var <- function(i,j,x,y,...) -var(residuals(lm(y[i:j]~x[i:j], ...)))
scoreij_cor <- function(i,j,x,y,...) cor(y[i:j], x[i:j], ...)
scoreij_r2 <- function(i,j,x,y,...) summary(lm(y[i:j]~x[i:j], ...))$r.squared
scoreij_r2adj <- function(i,j,x,y,...)
    summary(lm(y[i:j]~x[i:j],...))$adj.r.squared
## pre-calculated matrix - used as "y"
scoreij_matrix <- function(i,j,x,y,...) y[i,j]



## generic dynamic programing recursion 
## 
## Generic dynamic programing recursion, solving
## \deqn{S_j = max_{\substack{\\i \le j-\Ell_{\min}\\i \ge j-\Ell_{\max}}} S_{i-jumps} + score(i,j) - P}{S[j] = max(S[i-jumps] + score(i,j) -P)}.
## 
## This implementation is highly inefficent when used with actual scoring
## functions, but fast when used with a pre-calculated scoring matrix,
## passed via argument \code{y}. The function is mainly used to (a) define
## input and output of recursions to be compatible with the \code{dpseg}
## wrapper (TODO: use setGeneric/setMethod), (b) to quickly test
## new scoring functionss, or (c) use with pre-calculated scoring matrices.
recursion_generic <- function(x, y, maxl, jumps=0, P=0, minl=3, S0=1,
                              type="var", storev=FALSE, storem=FALSE,
                              scoref, ...) {

    ## use pre-calculated scoring matrix
    if ( class(y)=="matrix" ) {
        type <- "matrix"
        scoref <- scoreij_matrix
        x <- seq_len(nrow(y))
    }
    
    ## get scoring function
    if ( missing(scoref) )
        scoref <- get_scoreij(type)

    ## ALLOW DISCRETE JUMPS
    ## jumps=TRUE -> S[i-1] + score(i,j)
    ## jumps=FALSE -> S[i] + score(i,j)
    jumps <- as.numeric(jumps)
    
    ## initialize
    N <- length(x)
    S <- imax <- numeric(N) # intialized to 0
    S[] <- -P

    if ( missing(maxl) )
        maxl <- N 
            
    ## score matrix: store for educational movies or debugging,
    ## or re-use with type="matrix"
    SCR <- NULL
    if ( storem ) 
        SCR <- matrix(NA, nrow=N, ncol=N)
    
    ## RECURSION
    for ( j in 2:N) {

        irng <- (j-maxl):(j-minl) +1 
        irng <- irng[irng>0] # skip irng<1 at start 
        si <- rep(-Inf, maxl-minl+1) # TODO: restrict to irng

        for ( i in irng ) { 

            idx <- i - (j-maxl) 
            ## get score and S[i] vector
            scr <- scoref(i,j,x,y,...)
            si[idx] <- ifelse(i==1&jumps==1, S0, S[i-jumps]) + scr - P
            
            if ( storem ) SCR[i,j] <- scr
        }
        S[j] <- max(si)
        idx <- which.max(si)
        imax[j] <- idx + (j-maxl) 
    }
    list(imax=imax, S=S, SCR=SCR)
}

## efficient lin.reg. recursion
## note: scoref ignored, only for consistency with recursion_generic
## S[i-jumps] + score(i,j) 
recursion_linreg <- function(x, y, maxl, jumps=0, P=0, minl=3, S0=1,
                             type="var", storev=TRUE, storem=FALSE,
                             scoref, ...) {
    
    ## ALLOW DISCRETE JUMPS
    ## jumps=TRUE -> S[i-1] + score(i,j)
    ## jumps=FALSE -> S[i] + score(i,j)
    jumps <- as.numeric(jumps)

    ## initialize
    N <- length(x)
    S <- imax <- numeric(N) # init to 0
    S[] <- -P #*(1:minl)
    
    if ( missing(maxl) )
        maxl <- N 

    ## store matrix: only required for educational movies!
    SCR <- NULL
    if ( storem ) 
        SCR <- matrix(NA, nrow=N, ncol=N)

    ## initialize x2, y2, xy
    sy <- sx <- sy2 <- sx2 <- sxy <- rep(0, N)
    sx[1] <- x[1]
    sy[1] <- y[1]
    sx2[1] <- x[1]^2
    sy2[1] <- y[1]^2
    sxy[1] <- y[1]*x[1]
    
    if ( storev )
        islp <- icpt <- ivar <- irsq <- rep(NA, N)

    ## RECURSION
    for ( j in 2:N ) {

        ## incremental sums 
        sx[j] <-  sx[j-1]  + x[j]        # \sum x
        sy[j] <-  sy[j-1]  + y[j]        # \sum y
        sx2[j] <- sx2[j-1] + x[j]^2      # \sum x^2
        sy2[j] <- sy2[j-1] + y[j]^2      # \sum y^2
        sxy[j] <- sxy[j-1] + x[j]*y[j]   # \sum x*y

        ## sum of squares for i=1 
        n <- j
        Syy <- (sy2[j]) - (sy[j])^2/n
        ## Sxx = \sum x^2 - (\sum x)^2/n
        Sxx <- (sx2[j]) - (sx[j])^2/n
        ## Sxy = \sum x*y - sum(x)*sum(y)/n
        Sxy <- (sxy[j]) - (sx[j])*(sy[j])/n

        ## range of i values tested
        irng <- (j-maxl):(j-minl) + 1
        irng <- irng[irng>0] # skip negative ranges at start

        ## scoring vector
        si <- rep(-Inf, maxl+1-minl) # rep(-Inf,length(irng)) #
        if ( storev ) slpi <- cpti <- vari <- r2i <- cori <- si

        ## calculate all scores i->j
        for ( i in irng ) {

            idx <- i - (j-maxl) # si vector index
            
            ## sum of squares: variance & covariance
            ## subtract previous
            ij <- i - 1 # sum of squares subtraction index
            n <- j-ij   # segment length i:j
            
            ## ij==0 (at i=1 with jumps==FALSE) handled above
            if( ij>0 ) {
                ## Syy = \sum y^2 - (\sum y)^2/n
                Syy <- (sy2[j]-sy2[ij]) - (sy[j]-sy[ij])^2/n
                ## Sxx = \sum x^2 - (\sum x)^2/n
                Sxx <- (sx2[j]-sx2[ij]) - (sx[j]-sx[ij])^2/n
                ## Sxy = \sum x*y - sum(x)*sum(y)/n
                Sxy <- (sxy[j]-sxy[ij]) - (sx[j]-sx[ij])*(sy[j]-sy[ij])/n
            } 
            
            ## only for debug, can later be removed
            position <- paste0("i=",i,", j=",j, ", idx=",idx)
            if ( i<1 ) stop("wrong i: ", position)
            if ( idx==0 ) stop("wrong index: ", position)
            if ( n<minl ) warning("too short segment: ", position)


            ## slope and intercept from i+1 to j:
            slp <- Sxy/Sxx
            
            ## scoring function: -var vs. r2
            var <- -(Syy - Sxy*slp) / (n-1)
            r2 <- slp*slp*Sxx/Syy
            cor <- sqrt(r2)

            ## get score(i,j): vari or r2i!
            scr <- get(paste0(type), mode="numeric")

            ## S[i-jumps] + score(i,j) - P
            if ( Sxx!=0.0 ) 
                si[idx] <- ifelse((i-jumps)==0, S0, S[i-jumps]) + scr - P
            else warning("numeric cancellation: Sxx==0.0")

            ## store values
            if ( storev ) {
                ## intercept
                if ( ij>0 )
                    cpti[idx] <- ((sy[j]-sy[ij]) - slp*(sx[j]-sx[ij]))/n
                else 
                    cpti[idx] <- ((sy[j]) - slp*(sx[j]))/n
                slpi[idx] <- slp
                vari[idx] <- var
                r2i[idx] <- r2
                cori[idx] <- cor
            }
            ## store matrix
            if ( storem ) SCR[i,j] <- scr
        }
        
        ## GET MAXIMUM SCORE
        ## S[j] = max S[i-jumps] + score(i,j)
        S[j] <- max(si)
        idx <- which.max(si)

        ## reverse max?
        ridx <- length(si) - which.max(rev(si)) + 1
        if ( idx!=ridx & j >= minl) 
            warning("multiple max at j: ", j, ",", idx,", ", ridx)

        ## ... store which i yielded maximum
        imax[j] <- idx + (j-maxl)
        
        ## store recorded values
        if ( storev ) {
            ## correct sign for variances
            vari <- -vari
            ivar[j] <- vari[idx]
            islp[j] <- slpi[idx]
            icpt[j] <- cpti[idx]
            irsq[j] <- r2i[idx]
        }
    }

    ## store recorded values
    values <- NULL
    if ( storev ) values <- list(icpt=icpt, islp=islp, ivar=ivar, irsq=irsq)

    list(imax=imax, S=S, SCR=SCR, values=values)
}


### NOTE: outdated first version, kept for demonstration purposes
## for recursion_old S[i] + score[i+jumps][j]
get_scorexy <- function(type) 
    get(paste0("scorexy","_",type), mode="function")
scorexy_var <- function(x, y, ...) -var(residuals(lm(y~x, ...)))
scorexy_cor <- function(x, y, ...) cor(y, x, ...)
scorexy_r2 <- function(x, y, ...) summary(lm(y~x, ...))$r.squared
scorexy_r2adj <- function(x, y, ...) summary(lm(y~x,...))$adj.r.squared

## NOTE: S0 has no effect, kept for consistency
## S[i] + score(i+jumps,j) 
recursion_old <- function(x, y, maxl, jumps=0, P=0, minl=3, S0, type="var",
                          storev=FALSE, storem=FALSE, scoref, ...) {

    if ( missing(scoref) )
        scoref <- get_scorexy(type)

    ## ALLOW DISCRETE JUMPS
    ## jumps=TRUE -> score(i+1,j)
    ## jumps=FALSE -> score(i,j)
    jumps <- as.numeric(jumps)
    
    ## initialize
    N <- length(x)
    S <- imax <- numeric(N) # init to 0
    imax[] <- 1 # TODO: required?
    S[] <- -P

    if ( missing(maxl) )
        maxl <- N - jumps
            
    ## store matrix: only required for educational movies!
    SCR <- NULL
    if ( storem ) 
        SCR <- matrix(NA, nrow=N, ncol=N)

    ## RECURSION
    for ( j in 2:N) {

        irng <- (j-maxl):(j-minl) +1-jumps # shift range
        irng <- irng[irng>0] # skip irng<1 at start 
        si <- rep(-Inf, maxl-minl+1) # TODO: restrict to irng

        for ( i in irng ) { 

            idx <- i - (j-maxl) + jumps  # vector index
            scr <- scoref(x[(i + jumps):j], y[(i + jumps):j], ...)
            si[idx] <- S[i] + scr - P
            
            if ( storem ) SCR[i,j] <- scr
        }
        S[j] <- max(si)
        idx <- which.max(si)
        imax[j] <- idx + (j-maxl) - jumps
    }
    list(imax=imax, S=S, SCR=SCR)
}
