[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/dpseg)](https://cran.r-project.org/package=dpseg)
[![Downloads](https://cranlogs.r-pkg.org/badges/dpseg)](https://cran.r-project.org/package=dpseg)


# R package `dpseg`: piecewise linear segmentation by a simple dynamic programing algorithm


This is the development container for the R package `dpseg`. For more
information see (https://gitlab.com/raim/dpseg) and the R package
vignette.

The scripts generated for developing and testing dpseg are in folder
scripts, with test data in folders growth and yeast.

The directory `bugs` collects test data that reveal bugs.

## DEPRECATED

The file doc/dpseg.Rmd was the template for the package vignette.

The folder pkg contained the development git version of the package,
but on 20200730 the package was moved to a fresh repo at
(https://gitlab.com/raim/dpseg).
