

## CRAN Package, v0.1.1

* README.md math is not resolved when viewed online at CRAN
* add orcid ID of peter
* Vignette multicol/row figure is split into separate figures
in online vignette

* allow r2=1 interpretation of horizontal lines? -> DONT, ill-defined,
perfect fits are counted, while flat lines that are not perfect
hits will not be counted.
* Var(x)=0 or Var(y)=0: count cases and issued only one warning,
* allow custom scoring beyond r2/var/cor, ie. move the latter
to a function and allow to pass arbitrary functions based on
all calculated incremental values?

* finish sections on straight lines and Var(x)/Var(y)=0 in vignette.

## General

* generalize recursion/backtrace/sgtable/missing-values:
  use classes to indicate where which length correction is required,
  integrate sgtable and backtrace into one function and make
  segment start/end default result

* vignette for custom scoring, recursion/backtracing tests,
  define required signatures for recursion/scoring/backtracing,
  include Rcpp-compiled functions,

* length-dependent P to get a direct length penalty,

* use RNAseq in official example,

* replace use of ggplot2 in vignette by base R,

* store.matrix and re-use requires to remove NA and Inf values before

* CRITICAL, but only seen in certain situations??
Error in .Call(<pointer: (nil)>, x, y, maxl, jumps, P, minl, S0, type,  : 
  NULL value passed as symbol address
-> perhaps only happens when loading sourced functions !?

* analyze jumps size within ORF transcripts in yeast data,
find a heuristic to merge with karl segments

* consensus backtracking, integrate karl: only allow positive slopes
to restrict to 5'->3' degradation? and search specifically for areas
of decreasing pol density with negative slopes (can't be 3' degr)

* add unittests with testthat for all public functions:
use as in benchmark, test backtrace output between recursions,
test internal vs lm-derived values, test NA handling


* vignette: latex/pdf vs rmarkdown/html: the latter doesn't do equation
numbering and doesn't understand newcommands in \begin{equation}, while
markdown with pdf output does both
see https://www.r-bloggers.com/how-to-package-vignettes-in-plain-latex/
how to use plain latex for pdf vignettes
see https://bookdown.org/yihui/rmarkdown/r-package-vignette.html
why html vignettes are the preferred format

* more efficient version for equispaced x
* why can Sxx==0 , and how to handle it correctly?
see https://de.wikipedia.org/wiki/Verschiebungssatz_(Statistik)
* optimiziation loop to find a good P/minl, minimizing median variance?
* test breakpoint significance after Oosterban or Muggio
* calculate "confidence interval" from imax plateau?
* allow multiple scorefunctions, combine karl & dpseg; normalize different
fitscore functions and adjust penalities

# Useful Resources

* https://de.wikipedia.org/wiki/Einfache_lineare_Regression
* https://de.wikipedia.org/wiki/Empirische_Varianz
empirische/biased vs. theoretische/unbiased variance,
Bessel's correction
* https://de.wikipedia.org/wiki/Bestimmtheitsma%C3%9F#Als_quadrierter_Korrelationskoeffizient
* https://de.wikipedia.org/wiki/Einfache_lineare_Regression#Sch%C3%A4tzer_f%C3%BCr_die_Fehlervarianz
* http://www.ruhr-uni-bochum.de/imperia/md/content/mathematik3/lehre/ss09/statmethlehre2/teil2-regression220609.pdf
* https://www.johndcook.com/blog/standard_deviation/Tony F. Chan, Gene H. Golub,
* Sxx=0, https://de.wikipedia.org/wiki/Verschiebungssatz_(Statistik)
Randall J. LeVeque: Algorithms for computing the sample variance: analysis and recommendations. In: The American Statistician Vol. 37, No. 3 (Aug., 1983), S. 242–247
