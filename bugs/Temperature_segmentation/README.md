
Example sent by antoniomaria.coruzzolo@unimore.it which
caused session abort with option type="r2".
Fixed on 20201107 for both Sxx=0 and Syy=0 cases.

